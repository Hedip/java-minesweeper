import exceptions.PartieNonInitialiseeException;
import types.CaseIHM;
import types.PositionGrille;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ControleurDemineur implements MouseListener {

    private VueDemineur vue;
    private ModeleDemineur modele;
    private Etat state;
    private javax.swing.Timer timer;

    public enum Etat {
        AVANT_INIT, EN_JEU, FINI
    }

    public ControleurDemineur(VueDemineur v) {
        this.vue = v;
        this.modele = new ModeleDemineur(this.vue.getNombreLignes(), this.vue.getNombreColonnes(), this.vue.getNombreMines());
        this.state = Etat.AVANT_INIT;

        //////////////////////////////////////////////////////////
        //Timer du jeu la methode actionPerfomed est appelée toutes les 1000ms (1sec)
        //Je n'utilise pas de methode lambda car je n'aime pas ça
        this.timer = new javax.swing.Timer(1000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                vue.updateTimer();
            }
        });
        //////////////////////////////////////////////////////////
    }

    //Inutile dans mon cas precis
    public ControleurDemineur(int l, int c, int m) {
        this.vue = new VueDemineur(l,c,m);
        this.modele = new ModeleDemineur(this.vue.getNombreLignes(), this.vue.getNombreColonnes(), this.vue.getNombreMines());
        this.state = Etat.AVANT_INIT;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        CaseIHM b = (CaseIHM) e.getSource();
        switch (this.state) {
            case AVANT_INIT:
                this.modele.initialiseGrille(new PositionGrille(b.getLigne(),b.getColonne()));
                this.timer.start();
                //System.out.println(this.modele);
                try {
                    this.decouvrirToutVide(b.getLigne(),b.getColonne(), this.modele.getValeurCase(b.getLigne(),b.getColonne()));
                } catch (PartieNonInitialiseeException ex) {
                    ex.printStackTrace();
                }
                this.state = Etat.EN_JEU;
                break;
            case EN_JEU:

                try {
                    if( !this.modele.isCaseDecouverte(b.getLigne(), b.getColonne()) && e.getButton() == MouseEvent.BUTTON1 && !this.vue.isCaseMarquee(b.getLigne(),b.getColonne()) ) {
                        if(this.modele.getValeurCase(b.getLigne(), b.getColonne()) != 0) {
                            this.decouvrirCase(b.getLigne(), b.getColonne(), this.modele.getValeurCase(b.getLigne(),b.getColonne()));
                        } else {
                            this.decouvrirToutVide(b.getLigne(), b.getColonne(), this.modele.getValeurCase(b.getLigne(),b.getColonne()));
                        }
                        if(this.isJeuFini()) {
                            this.timer.stop();
                            JOptionPane.showMessageDialog(null, "Vous avez gagné !");
                            this.state = Etat.FINI;
                        } else if (this.modele.isCaseGrilleMine(b.getLigne(), b.getColonne()) && !this.vue.isCaseMarquee(b.getLigne(),b.getColonne())) {
                            this.timer.stop();
                            JOptionPane.showMessageDialog(null, "Vous avez perdu :(");
                            this.state = Etat.FINI;
                        }
                    }
                } catch (PartieNonInitialiseeException ex) {
                    ex.printStackTrace();
                }
                //System.out.println(this.modele);
                if(this.state != Etat.FINI) break;

            case FINI:
                try {
                    this.decouvrirTout();
                } catch (PartieNonInitialiseeException ex) {
                    ex.printStackTrace();
                }
                this.state = Etat.FINI;
                JOptionPane.showMessageDialog(null, "La partie est terminée.");
                break;
            default:
                break;
        }
    }

    //Toggle du marqueur quand le click vient juste d'être enfoncé (plus réactif)
    @Override
    public void mousePressed(MouseEvent e) {
        CaseIHM b = (CaseIHM) e.getSource();
        if(e.getButton() == MouseEvent.BUTTON3 && this.state == Etat.EN_JEU && !this.modele.isCaseDecouverte(b.getLigne(), b.getColonne())) {
            this.vue.toggleMarqueCase(b.getLigne(), b.getColonne());
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    private void decouvrirToutVide(int ligne, int colonne, int valeur) throws PartieNonInitialiseeException {
        //On decouvre les cases de façon recursive
        //On cherche les cases qui ne sont pas encore découvertes et qui ont pour valeur 0 (pas de mines adjacentes)
        //et on va découvrir toutes les cases autour de celles-ci
        //certains appel à "decouvrirCase" sont redondant cependant je n'ai pas trouvé de meilleur alternative
        //je pourrai check si la case est déjà découverte avant de faire un nouvel appel de "decouvrirCase" dessus
        if(!this.modele.isCaseDecouverte(ligne,colonne)) this.decouvrirCase(ligne, colonne, valeur);
        if(valeur == 0) {
            for(int loffset = -1; loffset <= 1; loffset++) {
                for (int coffset = -1; coffset <= 1; coffset++) {
                    if( (ligne + loffset) > -1 &&
                            (ligne + loffset) < this.vue.getNombreLignes() &&
                            (colonne + coffset) > -1 &&
                            (colonne + coffset) < this.vue.getNombreColonnes() ) {
                        if(!this.modele.isCaseDecouverte(ligne + loffset, colonne + coffset))
                            this.decouvrirToutVide(ligne + loffset, colonne + coffset, this.modele.getValeurCase(ligne + loffset,colonne + coffset));
                    }
                }
            }
        }
        //un jeu de demineur à un nombre de mine minimum si vous vous retrouvez dans le cas en dessous :
        //la partie peut se terminer dans l'état AVANT_INIT si il n'y a pas beaucoup de mines
        //cette condition n'est jamais remplie dans mon cas à moi
        //car il a le bon nombre de mine pour ne jamais finir la partie dans cet état là
        /* Ne pas oublier de reactiver cette condition pour des parties avec très peu de mines

        if(this.isJeuFini()) {
            this.decouvrirTout();
            this.state = Etat.FINI;
        }

        */

    }

    private void decouvrirCase(int i, int j, int valeur) {
        this.vue.afficherCase(i, j, valeur);
        this.modele.decouvrirCase(i, j);
    }

    private void decouvrirTout() throws PartieNonInitialiseeException {
        //utilisé en fin de partie
        //cette méthode va sûrement être changée pour aussi arrêter le timer
        for (int i = 0; i < this.vue.getNombreLignes(); i++) {
            for (int j = 0; j < this.vue.getNombreColonnes(); j++) {
                this.decouvrirCase(i, j, this.modele.getValeurCase(i,j));
            }
        }
    }

    private boolean isJeuFini() {
        for(PositionGrille pos : this.modele.getPositionsPasMines()) {
            if(!this.modele.isCaseDecouverte(pos.getLigne(), pos.getColonne())) return false;
        }
        return true;
    }

}
