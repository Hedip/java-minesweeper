import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

public class ControleurRejouer implements ActionListener {

    private FenetreDemineur fm;

    public ControleurRejouer(FenetreDemineur f) {
        this.fm = f;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem item = (JMenuItem) e.getSource();
        if(item.getText().equals("Rejouer")) {
            fm.creerNouvelleVueDemineur();
        }

    }

}
