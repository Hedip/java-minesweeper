import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import java.awt.Color;
import java.awt.Dimension;

public class FenetreDemineur extends JFrame {

    private VueDemineur panelJeuDemineur;
    private JMenuItem itemDifficulte;
    private final static Integer[] EXPERT = new Integer[]{30,30,180};
    private final static Integer[] NORMAL = new Integer[]{20,20,80};
    private final static Integer[] FACILE = new Integer[]{10,10,20};

    public FenetreDemineur() {

        this.setTitle("Démineur Projet P32 - Par Antoine MILHAU");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.panelJeuDemineur = new VueDemineur(FenetreDemineur.FACILE);
        this.add(this.panelJeuDemineur);
        this.setMinimumSize(new Dimension(this.panelJeuDemineur.getNombreLignes()*32,this.panelJeuDemineur.getNombreColonnes()*32+50));


        JMenuBar menuBar = new JMenuBar();
        menuBar.setBackground(Color.white);
            JMenu menuJeu = new JMenu("Jeu");
                JMenuItem menuItemRejouer = new JMenuItem("Rejouer");
                JMenuItem menuItemQuitter = new JMenuItem("Quitter");
                menuItemQuitter.addActionListener(new ControleurQuitter(this));
                menuItemRejouer.addActionListener(new ControleurRejouer(this));
            menuJeu.add(menuItemRejouer);
            menuJeu.add(menuItemQuitter);

        JMenu menuOptions = new JMenu("Options");
        JMenu menuDifficulte = new JMenu("Difficulté");
            ControleurDifficulte cD = new ControleurDifficulte(this);
            JMenuItem menuItemDifficulteExpert = new JMenuItem("Expert");
            menuItemDifficulteExpert.addActionListener(cD);
            JMenuItem menuItemDifficulteNormal = new JMenuItem("Normal");
            menuItemDifficulteNormal.addActionListener(cD);
            this.itemDifficulte = new JMenuItem("Facile");
            this.itemDifficulte.setBackground(Color.red);
            this.itemDifficulte.addActionListener(cD);
        menuDifficulte.add(menuItemDifficulteExpert);
        menuDifficulte.add(menuItemDifficulteNormal);
        menuDifficulte.add(this.itemDifficulte);



        menuOptions.add(menuDifficulte);
        menuBar.add(menuJeu);
        menuBar.add(menuOptions);
        this.setJMenuBar(menuBar);


        this.setVisible(true);

    }

    public void changerDifficulte(JMenuItem item) {
        this.itemDifficulte.setBackground(null);
        this.itemDifficulte = item;
        this.itemDifficulte.setBackground(Color.RED);
    }

    public void creerNouvelleVueDemineur() {
        this.remove(this.panelJeuDemineur);
        switch(this.itemDifficulte.getText()) {
            case "Expert":
                this.panelJeuDemineur = new VueDemineur(FenetreDemineur.EXPERT);
                break;
            case "Normal":
                this.panelJeuDemineur = new VueDemineur(FenetreDemineur.NORMAL);
                break;
            case "Facile":
                this.panelJeuDemineur = new VueDemineur(FenetreDemineur.FACILE);
                break;
        }
        this.add(this.panelJeuDemineur);
        this.setMinimumSize(new Dimension(this.panelJeuDemineur.getNombreLignes()*32,this.panelJeuDemineur.getNombreColonnes()*32+50));
        this.setVisible(true);
        this.repaint();
    }


}
