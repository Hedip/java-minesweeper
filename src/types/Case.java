package types;

public class Case {

    private int nbMinesVoisines;
    private boolean mine;
    private boolean revelee;

    public Case() {
        this.mine = false;
        this.nbMinesVoisines = 0;
        this.revelee = false;
    }

    public boolean isRevelee() {
        return this.revelee;
    }

    public void setRevelee(boolean b) {
        this.revelee = b;
    }

    public void setMine(boolean b) {
        this.mine = b;
    }

    public boolean isMine() {
        return this.mine;
    }

    public void setNbMinesVoisines(int nb) {
        this.nbMinesVoisines = nb;
    }

    public int getNbMinesVoisines() {
        return this.nbMinesVoisines;
    }

    @Override
    public String toString() {
        return (this.mine) ? "# ": (!this.revelee) ? this.nbMinesVoisines+" " : this.nbMinesVoisines+"'";
    }
}
