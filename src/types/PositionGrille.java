package types;

public class PositionGrille {

    private int ligne;
    private int colonne;

    public PositionGrille(int i, int j) throws IllegalArgumentException {
        this.ligne = i;
        this.colonne = j;
    }

    public int getLigne() {
        return this.ligne;
    }

    public int getColonne() {
        return this.colonne;
    }

    public void setLigne(int l) {
        this.ligne = l;
    }

    public void setColonne(int c) {
        this.colonne = c;
    }

    @Override
    public String toString() {
        return "("+
                this.ligne+
                ","+
                this.colonne+
                ")";
    }

    //Besoin de la methode equals pour remove dans les options
    @Override
    public boolean equals(Object o) {
        if(o == null) {
            return false;
        }
        if(!(o instanceof PositionGrille)) {
            return false;
        }
        PositionGrille other = (PositionGrille) o;
        return other.getLigne() == this.getLigne() && other.getColonne() == this.getColonne();
    }

}
