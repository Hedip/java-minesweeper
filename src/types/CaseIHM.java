package types;

import javax.swing.*;

public class CaseIHM extends JButton {

    private int ligne;
    private int colonne;

    public CaseIHM(int i, int j) {
        this.ligne = i;
        this.colonne = j;
    }

    public int getLigne() {
        return this.ligne;
    }

    public int getColonne() {
        return colonne;
    }




}
