import exceptions.PartieNonInitialiseeException;
import types.Case;
import types.PositionGrille;

import java.util.ArrayList;
import java.util.List;

public class ModeleDemineur {

    private Case[][] grille;
    private int nombreMines;
    private int nbLignes;
    private int nbColonnes;
    private boolean partieInitialisee;
    private List<PositionGrille> positionsPasMines;

    public ModeleDemineur(int nombreLignes, int nombreColonnes, int nbMines) throws IllegalArgumentException {
        //Si il y a plus de lignes ou autant que de cases dans le jeu renvoie une IllegalArgumentException
        //Tant que la position de depart est bien en 2 dimentions
        //on laisse aussi la place autour de la position de depart d'où le +8
        if(nbMines + 8 >= nombreLignes*nombreColonnes) throw new IllegalArgumentException();

        this.nbLignes = nombreLignes;
        this.nbColonnes = nombreColonnes;
        this.grille = new Case[this.nbLignes][this.nbColonnes];
        this.nombreMines = nbMines;
        this.positionsPasMines = new ArrayList<>();

    }

    public boolean isCaseGrilleMine(int l, int c) throws PartieNonInitialiseeException {
        if(!this.partieInitialisee) throw new PartieNonInitialiseeException();
        return this.grille[l][c].isMine();
    }

    public int getValeurCase(int l, int c) throws PartieNonInitialiseeException {
        if(!this.partieInitialisee) throw new PartieNonInitialiseeException();
        return this.grille[l][c].getNbMinesVoisines();
    }


    //On veut initialiser la partie avec une position de depart dans le sens ou on veut faire commencer
    public void initialiseGrille(PositionGrille posDepart) throws IllegalArgumentException {
        if(posDepart == null ||
           posDepart.getLigne() < 0 || //si position est negative
           posDepart.getLigne() > this.nbLignes || //si position superieur a la taille max
           posDepart.getColonne() < 0 ||
           posDepart.getColonne() > this.nbColonnes) throw new IllegalArgumentException("Position de depart invalide");
        //Instantiation de la grille
        for(int l = 0; l < this.grille.length; l++) {
            for(int c = 0; c < this.grille[l].length; c++) {
                this.grille[l][c] = new Case();
            }
        }

        //Creation de toutes les positions possible sur la grille
        List<PositionGrille> options = new ArrayList<>();

        for(int i = 0; i < this.nbLignes; i++) {
            for (int j = 0; j < this.nbColonnes; j++) {
                options.add(new PositionGrille(i,j));
            }
        }

        //Par securite on donne un perimetre de depart a lutilisateur la ou il va commencer
        //on supprime tout les options autour de ce depart
        for(int loffset = -1; loffset <= 1; loffset++) {
            for (int coffset = -1; coffset <= 1; coffset++) {
                if( (posDepart.getLigne() + loffset) > -1 &&
                        (posDepart.getLigne() + loffset) < this.nbLignes &&
                        (posDepart.getColonne() + coffset) > -1 &&
                        (posDepart.getColonne() + coffset) < this.nbColonnes ) {
                    PositionGrille optionAEnlever = new PositionGrille( posDepart.getLigne() + loffset,
                                                                        posDepart.getColonne() + coffset);
                    options.remove(optionAEnlever);
                }
            }
        }

        //On va generer le nombre de mines que l'utilisateur à demandé
        for(int b = 0; b < this.nombreMines; b++) {
            //on choisi un index aléatoire de tout les choix possibles
            int randomIndex = (int) Math.floor( Math.random() * options.size() );
            //on supprime ce choix des possibilités et on le recupère
            PositionGrille position = options.remove(randomIndex);
            //on met une mine à cette position
            this.grille[ position.getLigne() ][ position.getColonne() ].setMine(true);
        }
        //les options restantes sont les endroits où il n'y a pas de mines
        this.positionsPasMines = options;

        //On calcule le nombres de mines autour de chaque cases
        //et on affecte sa valeur à chaque case
        for(int i = 0; i < this.nbLignes; i++) {
            for (int j = 0; j < this.nbColonnes; j++) {
                this.grille[i][j].setNbMinesVoisines(this.compterMinesVoisines(i, j));
            }
        }

        //la partie a bien été initialisée
        this.partieInitialisee = true;
        //System.out.println(this.positionsPasMines);

    }


    ///////////////////////////////////////////////////////////////////////////////////
    //Duplication de code un peu inutile je voulais juste m'entrainer avec la surcharge
    ///////////////////////////////////////////////////////////////////////////////////
    public void initialiseGrille(int[] posDepart) throws IllegalArgumentException {
        if( posDepart == null ||
            posDepart.length != 2 || //si position pas en 2 dimentions
            posDepart[0] < 0 || //si position est negative
            posDepart [0] > this.nbLignes || //si position superieur a la taille max
            posDepart[1] < 0 ||
            posDepart[1] > this.nbColonnes) throw new IllegalArgumentException("Position de depart invalide");
        this.initialiseGrille(new PositionGrille(posDepart[0], posDepart[1]));
    }

    public void initialiseGrille(Integer[] posDepart) throws IllegalArgumentException {
        if( posDepart == null ||
            posDepart.length != 2 || //si position pas en 2 dimentions
            posDepart[0] < 0 || //si position est negative
            posDepart [0] > this.nbLignes || //si position superieur a la taille max
            posDepart[1] < 0 ||
            posDepart[1] > this.nbColonnes) throw new IllegalArgumentException("Position de depart invalide");
        this.initialiseGrille(new PositionGrille(posDepart[0], posDepart[1]));
    }
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////


    private int compterMinesVoisines(int i, int j) {
        //Si la case ou on essaye de calculer les mines voisines est une mine alors c'est inutile de compter
        if( this.grille[i][j].isMine() ) return -1;

        int total = 0;
        //on regarde les cases autour (et la case elle meme aussi mais elle ne pas etre une mine donc ça ne derange pas)
        for(int loffset = -1; loffset <= 1; loffset++) {
            for (int coffset = -1; coffset <= 1; coffset++) {

                //on vérifie bien que la case qu'on regarde existe dans la grille
                //(quelle soit pas négative ou supérieure à la taille du jeu)
                if( (i + loffset) > -1 &&
                    (i + loffset) < this.nbLignes &&
                    (j + coffset) > -1 &&
                    (j + coffset) < this.nbColonnes ) {
                    //et si cette case qu'on regarde autour est une mine on ajoute 1 au total des mines
                    if( this.grille[i + loffset][j + coffset].isMine() ) total++;
                }

            }
        }
        return total;
    }

    public boolean isCaseDecouverte(int i, int j) {
        return this.grille[i][j].isRevelee();
    }

    public void decouvrirCase(int i, int j) {
        this.grille[i][j].setRevelee(true);
    }


    public List<PositionGrille> getPositionsPasMines() {
        return this.positionsPasMines;
    }


    //les cases revelees sont affichees avec une apostrophe
    //utilisation d'un StringBuilder pour la création du toString
    //il serai aussi possible dans mon cas d'utiliser une boucle simplifiée pour le premier for
    @Override
    public String toString() {
        if(!this.partieInitialisee) return "Partie non initialisee";
        StringBuilder grilleString = new StringBuilder();
        for(int l = 0; l < this.grille.length; l++) {
            for(int c = 0; c < this.grille[l].length; c++) {
                grilleString.append(this.grille[l][c]);
                grilleString.append((c != this.grille[l].length-1) ? "| " : "");
            }
            grilleString.append("\n");
            for(int i = 0; i < this.nbColonnes*2-1; i++) grilleString.append("- ");
            grilleString.append("\n");
        }
        return grilleString.toString();
    }

}
