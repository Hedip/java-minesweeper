import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

public class ControleurQuitter implements ActionListener {

    private FenetreDemineur fm;

    public ControleurQuitter(FenetreDemineur f) {
        this.fm = f;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.exit(0);
    }

}