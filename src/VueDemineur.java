import types.CaseIHM;


import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class VueDemineur extends JPanel {

    private CaseIHM[][] grille; //Grille de boutons du jeu
    private int nombreLignes; //Nombre de lignes dans le jeu
    private int nombreColonnes; //Nombre de colonnes dans le jeu
    private int nombreMines; //Nombre de mines dans le jeu
    private ImageIcon marqueur; //Image du marqueur "drapeau" (sera enlevé dans le futur)
    private ImageIcon[] caseIcons;
    private int time; //Timer de la partie
    private JLabel timer;

    public VueDemineur(int nbLignes, int nbColonnes, int nbMines) {

        this.chargerImages();
        this.nombreLignes = nbLignes;
        this.nombreColonnes = nbColonnes;
        this.nombreMines = nbMines;
        /////
        this.setLayout(new BorderLayout());

        //////////////////////////////////////////////////////////
        //Panneau des informations du jeu :
        //Nombre de mines en JLabel à l'ouest
        //Bouton de redémarrage au centre (pas encore fonctionnel)
        //Timer de la partie à l'est
        JPanel infosJeu = new JPanel();
        infosJeu.setLayout(new BorderLayout());
            JButton jb = new JButton();
            jb.setBackground(Color.white);
            jb.setBorderPainted(false);
            jb.setPreferredSize(new Dimension(10,10));
        infosJeu.add(new JLabel("Mines : "+this.nombreMines), BorderLayout.WEST);
            this.timer = new JLabel("Timer : " + this.time);
        infosJeu.add(this.timer, BorderLayout.EAST);
        infosJeu.add(jb, BorderLayout.CENTER);
        infosJeu.setBackground(Color.white);
        //////////////////////////////////////////////////////////
        this.add(infosJeu, BorderLayout.NORTH);

        //////////////////////////////////////////////////////////
        ////// Grille de jeu au centre du panneau principal
        JPanel grilleJeu = new JPanel();
        grilleJeu.setBackground(Color.white);
        grilleJeu.setLayout(new GridLayout(nbLignes, nbColonnes));
        ControleurDemineur c = new ControleurDemineur(this);

        this.grille = new CaseIHM[nbLignes][nbColonnes];

        for (int i = 0; i < nbLignes; i++) {
            for (int j = 0; j < nbColonnes; j++) {
                CaseIHM b = new CaseIHM(i,j);
                this.grille[i][j] = b;
                b.setBackground(Color.lightGray);
                b.addMouseListener(c);
                grilleJeu.add(b);
            }
        }
        this.add(grilleJeu, BorderLayout.CENTER);
        //////////////////////////////////////////////////////////

        this.setVisible(true);
    }
    public VueDemineur(Integer[] i) {
        this(i[0], i[1], i[2]);
    }

    private void chargerImages() {
        this.caseIcons = new ImageIcon[9];
        try {
            this.marqueur = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("imgs/marqueur.png")));
            this.caseIcons[0] = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("imgs/bombe.png")));
            for(int i = 1; i < this.caseIcons.length; i++) {
                this.caseIcons[i] = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("imgs/"+i+".png")));
            }
        } catch (IOException e) {
            System.out.println("Erreur dans le chargement des images");
            e.printStackTrace();
        }
    }

    public void updateTimer() {
        this.time++;
        this.timer.setText("Timer : " + this.time);
    }

    public void afficherCase(int i, int j, int valeur) {
        //selon la valeur de la case on affichera une icone différente
        switch(valeur) {
            case 0: // pas de mines dans les cases adjacentes
                this.grille[i][j].setIcon(null);
                break;
            case -1: // la case est une mine
                this.grille[i][j].setIcon(this.caseIcons[0]);
                break;
            default: // on va cherche l'image correspondante au nombre de mines autour
                this.grille[i][j].setIcon(this.caseIcons[valeur]);
                break;
        }
        //dans tout les cas on va mettre le background en blanc comme case "découverte"
        this.grille[i][j].setBackground(Color.white);
    }


    public boolean isCaseMarquee(int i, int j) {
        //le marquage d'une carte est fait côté vue (mais ça va passer côter modele)
        return this.grille[i][j].getIcon() == this.marqueur;
    }

    public void toggleMarqueCase(int i, int j) {
        //si la case est marquée on la démarque et inversement
        if(this.isCaseMarquee(i,j))
            this.grille[i][j].setIcon(null);
        else
            this.grille[i][j].setIcon(this.marqueur);
    }

    public int getNombreLignes() {
        return this.nombreLignes;
    }

    public int getNombreColonnes() {
        return this.nombreColonnes;
    }

    public int getNombreMines() {
        return this.nombreMines;
    }
}
