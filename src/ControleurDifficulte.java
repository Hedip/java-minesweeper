import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

public class ControleurDifficulte implements ActionListener {

    private FenetreDemineur fm;

    public ControleurDifficulte(FenetreDemineur f) {
        this.fm = f;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem item = (JMenuItem) e.getSource();
        this.fm.changerDifficulte(item);
    }
}
